#include "CSCntuple/CSCntupleReader.h"
#include "TStopwatch.h"



#include <cstdlib>



void help(TString wrongArg = ""){
    std::cout << "No such argument: "<< wrongArg<< std::endl;

      std::cout << "-i =          input" << std::endl;
      std::cout << "-t =          treename" << std::endl;
      std::cout << "-n =          nevents" << std::endl;
      std::cout << "--sel =          SegmentSelection" << std::endl;
      std::cout << "--cut =          muonPtCut" << std::endl;
      std::cout << "-v =          verbose" << std::endl;
      std::cout << "-a =          useAllEvents" << std::endl;
      std::cout << "--EventDisplay =          EventDisplay" << std::endl;


    abort();


}


int main(int argc, char *argv[])
{

    //PlotOptions

    int nevents(-1);
    bool EventDisplay = false;


    double muonPtCut = 20000;

    TString output(getenv ("PlotDir"));
    output+=(timestamp()+"/CSCstudies/");
    std::vector<TString> input;
    bool verbose(0);
//     bool useAllEvents(0);
    TString treename = "muon";
    bool skipBookkeeping = false;
    bool useRefit = false;
    bool isMC = false;
    if (argc == 0 )help();

     for(int i = 1; i < argc; i++) {

        if (strcmp(argv[i], "-o")   == 0) {
            output = argv[++i];
            skipBookkeeping = true;
        }
        else if (strcmp(argv[i], "-i")  == 0)
            //~ while (std::string(argv[i+1]).at(0) != '-' )
            input.push_back(argv[++i]);
        else if (strcmp(argv[i], "-t")  == 0) treename = argv[++i];
        else if (strcmp(argv[i], "-n")  == 0) nevents = atoi(argv[++i]);
        else if (strcmp(argv[i], "--cut")  == 0) muonPtCut = atoi(argv[++i])*1000; //in gev
        else if (strcmp(argv[i], "-v")  == 0) verbose=true;
        else if (strcmp(argv[i], "--isMC")  == 0) isMC=true;
//         else if (strcmp(argv[i], "-a")  == 0) useAllEvents=true;
        else if (strcmp(argv[i], "--EventDisplay")  == 0) EventDisplay=true;
        else if (strcmp(argv[i], "--useRefit")  == 0) useRefit=true;
        //else if (strcmp(argv[i], "-o")  == 0) outdir = argv[++i];
        else help(argv[i]);
     }



    //bookkeeping derive outputname from input name
    if (!skipBookkeeping ){
    TString input_part = "";
    std::string inp = std::string(input[0]);
    size_t pos1 = inp.find("data/",0)+4;
    size_t len  = inp.find("_EXT",pos1)- pos1;
    input_part = inp.substr(pos1,len);
    output+=input_part;
    if(muonPtCut>0){
        std::stringstream ss;
        ss<< "." <<muonPtCut/1000 << "GeV";
        output+=ss.str();
    }
    output+="/";
    }



    std::cout << "=================================" << std::endl;
    std::cout << "Input: " ;
    if(!input.empty()) for (auto i : input) std::cout << i <<", " << std::endl;
    else std::cout  << std::endl;
    std::cout << "Treename: " << treename<< std::endl;
    std::cout << "Outputlevel: " << (verbose? "verbose": "quiet")<< std::endl;
    std::cout << "Output: " << output<< std::endl;
    std::cout << "Events: " << nevents<< std::endl;
    std::cout << "=================================" << std::endl;


    system("mkdir -p "+output);
    if(EventDisplay)system("mkdir -p "+output+"/EventDisplays");

    CSCntupleReader *reader = new CSCntupleReader();
    reader->m_showEventDisplays = EventDisplay;
    reader->m_useRefit = useRefit;
    reader->m_Sfits = true;
    reader->m_isMC = isMC;
    reader->SetPtCut(muonPtCut);
    reader->SetInputFile(input);
    reader->SetInputTree(treename);
    reader->SetVerbose(verbose);
    reader->SetOutputFile(output);
    reader->SetNevents(nevents);
//     reader->AllEvents(useAllEvents);
    TStopwatch *t = new TStopwatch ();
    reader->Process();
    t->RealTime ();
    return 0;
}
