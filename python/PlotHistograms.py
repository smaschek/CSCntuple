#!/usr/bin/env python3

import ROOT
from optparse import OptionParser


def PlotHistograms(opts, args):
    ROOT.gStyle.SetOptTitle(0);
    ROOT.gStyle.SetOptStat(0);
    rootfile = ROOT.TFile(opts.inputfile)
    ROOT.gROOT.SetBatch(1)
    Selections =["NoUnspoiled","1Unspoiled","2Unspoiled","3Unspoiled","4Unspoiled"]
    #,"1Unspoiled","2Unspoiled","3Unspoiled","4Unspoiled"]
    histnames  = {}
    typs = ["Muon","Hit"]
    histnames["Muon"]= ["ptresolution","relativeUncertainty"]
    histnames["Hit"]= ["sfit","sfit2D","largeOldPos","smallNewPos","smallOldPosModulo","smallNewPosModulo","smallOldPos","largeNewPos","largeOldPosModulo","largeNewPosModulo","OldVsNewPos","OldMinusNewPos"]
    Histos = {}
    Sum = {}
    for typ in typs:
        for name in histnames[typ]:
            print "plotting %s"%name
            Histos[name] = {}
            Sum[name] = 0
            for sel in reversed(Selections):
                histoname = "%s_%s_None_%s" % (typ,sel,name)
                histo = rootfile.Get(histoname)
                if not histo: continue
                is2D = histo.InheritsFrom("TH2")
                #if not histo : histo = rootfile.Get(histoname.replace("Muon","Hit")).Rebin(10)
                if not Sum[name] : 
                    Sum[name] = histo
                else:
                    Sum[name].Add(histo)
                Histos[name][sel]= Sum[name].Clone()
                Histos[name][sel].SetTitle(sel)
                #print "entries",sel,Sum[name].GetEntries()
                #if not Histos[name][sel] : print "no histo named %s" %histoname
            canvas = ROOT.TCanvas()
            i=11 
            if is2D:
                canvas.Print(opts.inputfile.replace(".root",".%s.pdf["%name))
            for sel in Selections:
                i+=1
                if not sel in Histos[name]: continue
                Histos[name][sel].SetLineColor(i)
                Histos[name][sel].SetFillColor(i)
                if not is2D:
                    if opts.log:
                        Histos[name][sel].GetYaxis().SetRangeUser(.5,Histos[name][sel].GetMaximum()**(1.5))
                    else:
                        Histos[name][sel].GetYaxis().SetRangeUser(.5,Histos[name][sel].GetMaximum()*(1.5))
                Histos[name][sel].Draw("histsamecolz")
                if is2D:
                    canvas.Print(opts.inputfile.replace(".root",".%s.pdf"%name))
                
                #print "entries",sel,Histo[name][sel].GetEntries()
            if opts.log:
                if not is2D:
                    canvas.SetLogy()
                else:
                    canvas.SetLogz()
            leg = canvas.BuildLegend(0.12,0.78,0.88,0.85)
            leg.SetNColumns(5)
            leg.SetBorderSize(0)
            if not is2D:
                canvas.Print(opts.inputfile.replace(".root",".%s.pdf"%name))
            else:
                canvas.Print(opts.inputfile.replace(".root",".%s.pdf]"%name))
            
        




if __name__ == "__main__":

    # parse command line input
    parser = OptionParser("usage: %prog [options] pattern [pattern2 pattern3 ...]")
    parser.add_option("-i", "--input", dest="inputfile", default="histograms.root", help="File containing histograms. Default=%default")
    parser.add_option("-o", "--output", dest="outputfile", default="histograms.root", help="File containing histograms. Default=%default")
    parser.add_option("", "--log",action="store_true", default=False, help="Plotting logy. Default=%default")

    opts, args = parser.parse_args()
    PlotHistograms(opts, args)
