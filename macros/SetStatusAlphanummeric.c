const char *labels[20]  = {
"Unspoiled",
"Simple",
"Edge",
"MultiPeak",
"Narrow",
"Wide",
"Skewed",
"QratInconsistent",
"StripFitFailed",
"Saturated",
"SplitUnspoiled",
"SplitSimple",
"SplitEdge",
"SplitMultiPeak",
"SplitNarrow",
"SplitWide",
"SplitSkewed",
"SplitQratInconsistent",
"SplitStripFitFailed"



};

void SetStatusAlphanummeric(TH1*h){
      for (uint i=1;i<=20;i++) h->GetXaxis()->SetBinLabel(i,labels[i-1]);
}
