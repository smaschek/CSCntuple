#include "CSCntuple/Segment.h"

#include <TCanvas.h>

//  
#include <TCanvas.h>
#include <TPad.h>
#include <TFile.h>
#include <TChain.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TF1.h>
#include <TLorentzVector.h>
#include "TGraphAsymmErrors.h"
#include "TGraphErrors.h"
#include "TMultiGraph.h"
#include "TMarker.h"
#include "TText.h"
#include "TPad.h"
#include "TLegend.h"
#include "TLatex.h"
#include <sstream>
//~ #include "CSCntuple/CustomStyle.C"



//constructor
Segment::Segment(muon* mu,uint seg_it,bool debug,bool useRefit)

: m_NSpoiled(0),m_NUnspoiled(0)
{
    Muon = mu;
    SegmentNumber = seg_it;
    n_Hits = Muon->seg_sfits.At(seg_it).size();
    m_sector = Muon->seg_sector.At(seg_it);
    m_useRefit = useRefit;
    SetTrack();
    SetHits();
    SetPosition();
    m_debug = debug;
    if (m_debug) this->Dump();

}
Segment::~Segment(){
    for (auto h : m_Hits) delete h;
    delete m_Track;
}

void Segment::SetTrack(){
    m_Track = new Track( Muon, Muon->segToMuon.At(SegmentNumber));
}

void Segment::SetPosition(){
    SetEta();
    SetPhi();
}

void Segment::SetEta(){
    double eta = 0;
    uint neta = 0;
    for (auto hit : m_Hits){
        eta+=hit->pstrip;
        neta++;
    }
    m_hit_strip_eta = eta/neta;
}

void Segment::SetPhi(){
    double phi = 0;
    uint nphi = 0;
    for (auto hit : m_Phi_Hits){
        phi+=hit->pstrip;
        nphi++;

    }
    m_hit_strip_phi = phi/nphi;
}

void Segment::SetHits(){
    for (uint It = 0 ; It < Muon->hit_sfit.GetSize();++It){
        if(Muon->hitToMuon.At(It) != SegmentNumber)continue;
        if(!Muon->hit_measphi->at(It) ){
            m_Hits.push_back(new Hit(Muon,It,this));
            if (m_useRefit){
                if(Muon->hit_srefit.At(It) > 1) m_NSpoiled++;
                else if (Muon->hit_srefit.At(It)  == 0) m_NUnspoiled++;
            }else{
                if(Muon->hit_sfit.At(It) > 1) m_NSpoiled++;
                else if (Muon->hit_sfit.At(It)  == 0) m_NUnspoiled++;
            }
        }else{
            m_Phi_Hits. push_back(new Hit(Muon,It,this));
        }
    }


}

void Segment::Dump(){
    std::cout << "============================================================"<< std::endl;
    std::cout << "Segment '"<< SegmentNumber <<"' "<< std::endl;
    std::cout << "avg pstrip eta "<<m_hit_strip_eta<< std::endl;
    std::cout << "avg pstrip phi "<<m_hit_strip_phi<< std::endl;
    std::cout << "sector         "<<m_sector<< std::endl;
    std::cout << "Track:"<< std::endl;
    m_Track->Dump();
    std::cout << "Hits (unspoiled|spoiled):"<< m_Hits.size()<<"("<< m_NUnspoiled <<"|"<< m_NSpoiled <<")"<<std::endl;
    //~ for (auto hit :m_Hits) hit ->Dump();
    std::cout << "============================================================"<< std::endl;
}





