#include "CSCntuple/CSCntupleReader.h"
#include "CSCntuple/Segment.h"
#include "CSCntuple/CustomStyle.C"


#include <iostream>
#include "TGraphAsymmErrors.h"
#include "TGraphErrors.h"
#include "TMultiGraph.h"
#include "TMarker.h"
#include "TText.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TLegend.h"
#include "TLine.h"
#include "TLatex.h"

#include <sys/stat.h>
#include <dirent.h>
#include <sys/types.h>
#include <stdio.h>



CSCntupleReader::CSCntupleReader() :
    m_debug(0),
    m_showEventDisplays(0),
    m_Sfits(0),
    m_muonPtCut(-1)
{

    if(m_debug) std::cout << "CSCntupleReader: Constructor"<< std::endl;

}

CSCntupleReader::~CSCntupleReader(){

   if(m_debug) std::cout << "CSCntupleReader: Destructor"<< std::endl;

}






void CSCntupleReader::Process(){
    Initialize();
    std::cout << "Found "<< total_entries<< " events" << std::endl;
    while(Muon->fReader.Next()){
        if (Muon->fReader.GetCurrentEntry () > m_nevents && m_nevents >0) break;

        Execute();
    }

    Finalize();

}



void CSCntupleReader::Initialize(){



    if (!m_debug) gErrorIgnoreLevel = kError;
    else gErrorIgnoreLevel = kWarning;
    if(m_debug) std::cout << "CSCntupleReader::Initialize()" << std::endl;
    size_t pos = std::string(m_inputFileName.at(0)).find("*");
    //~ if(m_debug) std::cout <<m_inputFileName.at(0)<< "pos " << pos << std::endl;
    if (pos != std::string::npos){    // load tree from several files via wildcard
        if(m_debug) std::cout << "load tree from several files via wildcard" << std::endl;
        m_inChain = new TChain(m_TreeName);
        m_inChain->Add(m_inputFileName.at(0));
        m_Tree = (TTree*) m_inChain;
    }else{                                          // load tree from one file
        if(m_debug) std::cout << " load tree from one file" << std::endl;
        m_inFile = TFile::Open(m_inputFileName.at(0), "READ");
        m_Tree = (TTree*)m_inFile->Get(m_TreeName);
    }


    HistInitialize();

    if(m_debug) std::cout << "Initialize muon tree reader" << std::endl;
    Muon = new muon();
    Muon->Init(m_Tree);
    total_entries = Muon->fReader.GetEntries(1);

}





void CSCntupleReader::HistInitialize(){
    if(m_debug) std::cout << "CSCntupleReader::HistInitialize()" << std::endl;
    
        //Histograms
    m_histSvc = new HistSvc();
    m_histNameSvc = new HistNameSvc();
    m_histSvc->SetHistNameSvc(m_histNameSvc);
}

void CSCntupleReader::Execute(){

    int entry = Muon->fReader.GetCurrentEntry ();
    if(entry % 10000 == 0 )std::cout <<"entry "<< entry << "/" << total_entries << std::endl;
    uint n_elemtens = Muon->hit_sfit.GetSize();
    if (!n_elemtens)return ;

    //~ SetTracks();// filles the variable m_TrackMuons: track pt/eta of the ID track muons
    //~ if (!passEventSelection()) return; // currently does nothing
    //~ for (uint hit = 0 ; hit < Muon->hit_sfit.GetSize() ; ++hit ){// counts the number of total hits
        //~ n_hits++;
        //~ if(999!=HitToSeg(hit))n_assoc_hits++; // and counts the number of hits assotiatable to a segment
    //~ }

    GetSegments();

    /// loop over the segments
    for(auto seg : m_Segments){
        //~ if (!passSegmentSelection(seg)) continue;

        if(m_debug)seg->Dump();




        switch ( seg->m_NUnspoiled ) {
            case 0 :
                m_histNameSvc->setSelection("NoUnspoiled");
                break;
            case 1 :
                m_histNameSvc->setSelection("1Unspoiled");
                break;
            case 2 :
                m_histNameSvc->setSelection("2Unspoiled");
                break;
            case 3 :
                m_histNameSvc->setSelection("3Unspoiled");
                break;
            case 4 :
                m_histNameSvc->setSelection("4Unspoiled");
                break;
            default :
                continue;
        }


        //turn event displays on with --EventDisplay
        ShowEventDisplay(seg->SegmentNumber);
        FillHistograms(seg);
        continue;


    }
}

void CSCntupleReader::ShowEventDisplay(uint segment){

    if (!m_showEventDisplays) return;
    gROOT->Macro("macros/CustomStyle.C");
    if(m_debug) std::cout << "CSCntupleReader::ShowEventDisplay("<<segment<<")" << std::endl;

    int sector = Muon->seg_sector.At(segment);

    if(m_debug) std::cout << "sector is " << sector << std::endl;

    uint n_Cluster = Muon->ClusterShape.GetSize();
    TCanvas* canvas = new TCanvas ("EventDisplay","CSC Event display",800,1000);
    TPad *p_up = new TPad ("up","up",0.,.4,1,1,-1,0);
    TPad *p_down = new TPad ("down","down",0.,0,1,.4,-1,0);


    std::stringstream sectornumber;
    sectornumber<<sector;
    TH2* h = new TH2D("EventDisplay",TString("Sector ")+sectornumber.str(),197,-1.5,195.5,33,0.5,7.1);
    TH2* hetaphi = new TH2D("etaphi",TString("Sector ")+sectornumber.str(),197,-1.5,195.5,51,-0.5,50.5);

    std::vector <TH1*> HitProfiles;
    //~ std::vector <TMarker*> HitPositions;
    TGraphErrors* HitPositions = new TGraphErrors(10);
        HitPositions->SetLineColor(1);
        HitPositions->SetLineWidth(2);
        HitPositions->SetFillColor(0);
        HitPositions->SetMarkerColor(1);
        HitPositions->SetMarkerStyle(5);
        HitPositions->SetMarkerSize(3);

    TMultiGraph* HitPositions1D = new TMultiGraph();
    std::vector <TText*> HitSFit;
    std::map <uint, TText*> Texts_pt;
    //~ for (uint i = 0 ; i <  m_TrackMuons.size();++i)Texts_pt.push_back(nullptr);

    //looping over the clusters

    for(uint It = 0; It < n_Cluster; ++It){

        int nstr = Muon->hit_nstr.At(It);
        int    sec         = Muon->hit_sector.At(It);
        if (Muon->hit_measphi->at(It)) continue;
        double pitchsize =  (sec%2==0)?5.5566:5.308;
        double pos          = Muon->hit_pos.At(It);
        double dpos          = Muon->hit_dpos.At(It);
        if(m_debug) std::cout << "cluster " << It << std::endl;
        pos = pos/pitchsize+96.5;
        std::vector <float> ClusterShape     = Muon->ClusterShape.At(It);
        int layer = Muon->hit_wlay.At(It);
        uint bin =  Muon->hit_str0.At(It);
        int nbins = nstr+5;
        int stripmin = bin - 3;
        int stripmax = stripmin + nbins +1;
        TH1* h1 = new TH1D(TString("ProfileHit")+std::to_string(It),TString("Hit ")+std::to_string(It)+TString("(Layer ")+std::to_string(layer)+")",nbins,stripmin,stripmax);
        h1->SetLineColor(NiceColor(layer));
        HitProfiles.push_back(h1);
        for (auto charge : ClusterShape ){
            h->Fill(bin,layer,charge);
            h1->Fill(bin,charge);
            bin++;
        }

        HitPositions->SetNameTitle((std::to_string(layer)+"_layer_reco").c_str(),"Reconstructed Position");
        uint point_index = HitPositions->GetN();
        HitPositions->SetPoint(point_index, pos,layer);
        HitPositions->SetPointError(point_index,dpos, 0);

        TGraphErrors* mar1D = new TGraphErrors(1);
        mar1D->SetNameTitle((std::to_string(layer)+"_layer_reco").c_str(),"Reconstructed Position");
        mar1D->SetPoint(0,pos, 1e7);
        mar1D->SetPointError(0,dpos, 0);
        mar1D->SetLineColor(h1->GetLineColor());
        mar1D->SetLineWidth(2);
        mar1D->SetFillColor(0);
        mar1D->SetMarkerColor(h1->GetLineColor());
        mar1D->SetMarkerStyle(5);
        mar1D->SetMarkerSize(3);
        HitPositions1D->Add(mar1D);

        // printing sfit values
        TLatex *t    = new TLatex(pos+8, layer-.3,TString("fit=")+TString(std::to_string(Muon->hit_sfit.At(It)))+TString(", refit=")+TString(std::to_string(Muon->hit_srefit.At(It))));

        // Printing the track particle pt
        std::stringstream MuonExtrapolatedPt,InnerDetectorExtrapolatedPt,MuonPt,TruthMuonPt,Resolution;
        uint n_muon = Muon->hitToMuon.At(It);
        MuonExtrapolatedPt << Muon->trk_me_pt.At(n_muon);
        InnerDetectorExtrapolatedPt << Muon->trk_id_pt.At(n_muon);
        MuonPt << Muon->trkPt.At(n_muon);
        TruthMuonPt << Muon->truth_pt.At(n_muon);
        Resolution<<  (Muon->trkPt.At(n_muon)-Muon->truth_pt.At(n_muon))/Muon->truth_pt.At(n_muon);
        TLatex* test_pt = new TLatex(pos, 5.3,TString(
            //~ "#splitline{#it{p}_{T}^{me} = "+MuonExtrapolatedPt.str()+" GeV"+
        //~ ",   #it{p}_{T}^{id} = "+InnerDetectorExtrapolatedPt.str()+" GeV}{"+
            //~ "#it{p}_{T}^{truth} = "+TruthMuonPt.str()+" GeV"+
        ",   #it{p}_{T}^{comb} = "+MuonPt.str()+" GeV, ")+
        "resolution = " +Resolution.str()
        );

        //~ std::cout<< MuonExtrapolatedPt.str()<<", "<< InnerDetectorExtrapolatedPt.str()<< std::endl;

        if(Texts_pt.find(n_muon)==Texts_pt.end()) Texts_pt.insert(std::make_pair(n_muon,test_pt));
        HitSFit.push_back(t);
    }

    // print the pdf files
    // cross section view
    p_up->cd();
    if (h->GetEntries()) {
        h->SetMinimum(1e3);
        h->SetMaximum(1e7);
        h->Draw("colz");
        h->GetYaxis()->SetNdivisions(10);

        for (auto t : HitSFit ){
            t->SetTextAlign(22);
            t->SetTextColor(1);
            t->SetTextFont(43);
            //~ t->SetTextSize(25);
            t->SetTextSize(10);
            t->SetTextAngle(15);
            t->Draw();
        }
        for (auto pair :  Texts_pt){
            auto t = pair.second;
            if (!t)continue;
            t->SetTextAlign(22);
            t->SetTextColor(1);
            t->SetTextFont(43);
            t->SetTextSize(10);
            t->SetTextAngle(90);
            t->Draw();
        }

        std::stringstream eventNsector;
        eventNsector << "event"<< *(Muon->evt) << "_sector"<< sectornumber.str();
        TLatex *t    = new TLatex(20, 6.5,eventNsector.str().c_str());
        t->SetTextSize(10);
        t->Draw();
        p_up->SetLogz();

        // hit profiles (1D)

        p_down->cd();
        int mini = 999;
        int maxi = 0;
        for (auto h1 : HitProfiles){
            mini = std::min(double(mini),h1->GetXaxis()->GetXmin());
            maxi = std::max(double(maxi),h1->GetXaxis()->GetXmax());
        }
        TH1D*axes= new TH1D("Profile","Profile",maxi-mini+1,double(mini)-.5,double(maxi)+.5);
        axes->SetMinimum(1e4);
        axes->SetMaximum(1e9);
        axes->Draw();
        for (auto h1 : HitProfiles){
            h1->SetLineWidth(3);
            h1->SetMarkerSize(0);
            h1->Draw("histsame");
        }
        HitPositions1D->Draw("CP");
        // keep this !!!
        //~ TLegend *l = p_down->BuildLegend(0.2,0.75,0.8,0.9);
        //~ l->SetBorderSize(0);
        p_down->SetLogy();

        canvas->cd();
        p_up->Draw();
        p_down->Draw();
        TString filename = m_outputFileName+"/EventDisplays/"+eventNsector.str()+".pdf";
        if(m_debug)std::cout << "print "<<filename<<"\n";
        canvas->Print (filename);
        delete axes;

        // front view histograms
        /*
        can2->cd();
        hetaphi->Draw("colz");
        TLatex* caption2 = new TLatex;
        caption2->DrawLatexNDC(0.01, 0.01, "\"Chamber front view\"");
        //~ can2->Print (filename+"_"+".3.eps");
        can2->Print (filename+".pdf");


        can->Print (filename+".pdf]");

        */
    }


    for (auto h1 : HitProfiles) delete h1;
    delete h;
    delete hetaphi;
    //~ delete can;
    //~ delete can1;
    //~ delete can2;


}

void CSCntupleReader::FillHistograms(Segment* segment){
    if(m_debug) std::cout << "CSCntupleReader::FillHistograms(Segment)" << std::endl;

    uint n_muon = Muon->segToMuon.At(segment->SegmentNumber);

    float MuonExtrapolatedPt = Muon->trk_me_pt.At(n_muon);
    float MuonExtrapolatedPterr = Muon->trk_me_pterr.At(n_muon);
//     float InnerDetectorExtrapolatedPt = Muon->trk_id_pt.At(n_muon);
//     float MuonPt = Muon->trkPt.At(n_muon);
    float TruthMuonPt = Muon->truth_pt.At(n_muon);
    float Resolution =  (MuonExtrapolatedPt-Muon->truth_pt.At(n_muon))/TruthMuonPt;
    m_histNameSvc->setObjectType("Muon");
    m_histSvc->BookFillHist("ptresolution",       std::string(m_histNameSvc->getSelection())+";pt resolution ",500, -2,2, Resolution , 1);
    m_histSvc->BookFillHist("relativeUncertainty",       std::string(m_histNameSvc->getSelection())+";relative p_{T} uncertainty ",500, 0,4,MuonExtrapolatedPterr/MuonExtrapolatedPt  , 1);
    m_histSvc->BookFillHist("unspoiledhits",       std::string(m_histNameSvc->getSelection())+";unspoiled hits  ",5, -.5,4.5,segment->m_NUnspoiled , 1);


    m_histNameSvc->setObjectType("Hit");
    for (uint It = 0 ; It < Muon->hit_sfit.GetSize();++It){
        if(Muon->hitToMuon.At(It) != segment->SegmentNumber)continue;
        if(Muon->hit_measphi->at(It) )continue;
        
        int sfit = Muon->hit_sfit.At(It);
        int srefit = Muon->hit_srefit.At( It);
        
        m_histSvc->BookFillHist("sfit",       std::string(m_histNameSvc->getSelection())+";sfit ",20, -.5,19.5, sfit , 1);
        m_histSvc->BookFillHist("sfit2D",       std::string(m_histNameSvc->getSelection())+";sfit;srefit ",20, -.5,19.5,20, -.5,19.5, sfit, Muon->hit_srefit.At(It) , 1);
        if(Muon->hit_srefit.At( It)!=0 ) 
        {
            if(m_debug)std::cout <<"sfit == "<< sfit <<", pos = "<<Muon->hit_pos.At( It) <<std::endl;
            continue;
        }
        m_histSvc->BookFillHist("OldVsNewPos",       std::string(m_histNameSvc->getSelection())+";fit position;refit position  ",300,-600,600,300,-600,600,Muon->hit_pos.At( It),Muon->hit_posrefit.At( It), 1);
        if(sfit!= 0 ){
            if(m_debug)std::cout <<"srefit == "<< srefit <<", pos = "<<Muon->hit_pos.At( It) <<"" <<std::endl;
            continue;
        }
        m_histSvc->BookFillHist("OldMinusNewPos",       std::string(m_histNameSvc->getSelection())+";fit - refit position  ",300,-0.3,.3,Muon->hit_pos.At( It)-Muon->hit_posrefit.At( It), 1);
        if (segment->m_sector %2 ==0)continue;
        if (std::fabs(Muon->hit_posrefit.At( It)-Muon->hit_pos.At( It))>0.2) continue;
        m_histSvc->BookFillHist("OldPos",       std::string(m_histNameSvc->getSelection())+";fit position in mm",100,0,53.08,Muon->hit_pos.At( It), 1);
        m_histSvc->BookFillHist("NewPos",       std::string(m_histNameSvc->getSelection())+";refit position in mm",100,0,53.08,Muon->hit_posrefit.At( It), 1);
    }
}

bool CSCntupleReader::passSegmentCriteria(Segment* segment){
    //~ return true; //hackhack
    if (segment->m_Track->P.Pt() < 25000) return false;
//     if (std::fabs(Muon->trk_me_eta.At(Muon->segToMuon.At(segment->SegmentNumber))) <2.5) return false;
    //~ if(m_debug)segment->Dump();
    //~ for (auto hit: segment->m_Hits){ // requires a hit where refit is better than fit
        //~ if (hit->sfit >1 && hit->srefit ==0) {
            //~ std::cout << hit->wlay<< ":  hit->sfit = " << hit->sfit <<", " << "hit->srefit = " << hit->srefit <<std::endl;
            //~ return true;
        //~ }
    //~ }
    //~ return false
    return true;
}

void CSCntupleReader::GetSegments(){
    for (auto oldsegment : m_Segments) delete oldsegment;
    m_Segments.clear();
    //loop over all segments in the Tree
    for (uint seg = 0 ; seg < Muon->seg_pstrips.GetSize();++seg){
        Segment *segment = new Segment(Muon,seg,0,m_useRefit);//
        if(passSegmentCriteria(segment))
            m_Segments.push_back(segment);
        else
            delete segment;
    }
}

void CSCntupleReader::Finalize(){
    if(m_debug) std::cout << "CSCntupleReader::Finalize()" << std::endl;
    std::cout<< "print to file "<< m_outputFileName+"/histograms.root"<<std::endl;
    TFile* outfile = new TFile(m_outputFileName+"/histograms.root","recreate");
    m_histSvc->Write(outfile);
    outfile->Close();
    return;
}




