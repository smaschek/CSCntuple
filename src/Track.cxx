#include "CSCntuple/Track.h"

#include <TCanvas.h>
 

#include <TCanvas.h>
#include <TPad.h>
#include <TFile.h>
#include <TChain.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TF1.h>
#include <TLorentzVector.h>
#include "TGraphAsymmErrors.h"
#include "TGraphErrors.h"
#include "TMultiGraph.h"
#include "TMarker.h"
#include "TText.h"
#include "TPad.h"
#include "TLegend.h"
#include "TLatex.h"
#include <sstream>
//~ #include "CSCntuple/CustomStyle.C"




Track::Track(muon* mu, uint TrackNumber)
: P()
{

    m_TrackNumber = TrackNumber;
    P.SetPxPyPzE(mu->trkPx.At(TrackNumber),mu->trkPy.At(TrackNumber),mu->trkPz.At(TrackNumber),0);
}


void Track::Dump(){
    std::cout << "This Track '"<< m_TrackNumber <<"' has following variables"<< std::endl;

    std::cout << "pt     "<<P.Pt()<< std::endl;
    std::cout << "eta    "<<P.Eta()<< std::endl;
    std::cout << "phi     "<<P.Phi()<< std::endl;
    //~ std::cout << "sector     "<<m_sector<< std::endl;
}

Track::~Track(){
}
