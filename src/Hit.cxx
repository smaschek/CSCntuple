#include "CSCntuple/Hit.h"

#include <TCanvas.h>

 
#include <TCanvas.h>
#include <TPad.h>
#include <TFile.h>
#include <TChain.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TF1.h>
#include <TLorentzVector.h>
#include "TGraphAsymmErrors.h"
#include "TGraphErrors.h"
#include "TMultiGraph.h"
#include "TMarker.h"
#include "TText.h"
#include "TPad.h"
#include "TLegend.h"
#include "TLatex.h"
#include <sstream>
//~ #include "CSCntuple/CustomStyle.C"




Hit::Hit(muon* mu, uint hit_it,Segment* segment){

    HitNumber = hit_it;
    Muon = mu;
    m_segment = segment;
    time = Muon->hit_time.At(hit_it);
    measphi = Muon->hit_measphi->at(hit_it);
    m_sector = Muon->hit_sector.At(hit_it);
    Cluster = Muon->ClusterShape.At(hit_it);
    nstr     = Muon->hit_nstr.At(hit_it);
    sfit     = Muon->hit_sfit.At(hit_it);
    srefit     = Muon->hit_srefit.At(hit_it);
    srefit   = Muon->hit_srefit.At(hit_it);
    qleft        = Muon->hit_qleft.At(hit_it);
    qright       = Muon->hit_qright.At(hit_it);
    qsum         = Muon->hit_qsum.At(hit_it);
    wlay = Muon->hit_wlay.At(hit_it);
    //  qleftsum3    = Muon->hit_qleftsum3.At(hit_it);
    //  qleftsum     = Muon->hit_qleftsum.At(hit_it);
    //  qrightsum3    = Muon->hit_qrightsum3.At(hit_it);
    //  qrightsum     = Muon->hit_qrightsum.At(hit_it);
    qpeak        = Muon->hit_qpeak.At(hit_it);
    //  peakiness    = (qleft+qright)/qpeak;
    // std::vector <> distance2segments;
    pitchsize =  (m_sector%2==0)?5.5566:5.308;
    pos          = Muon->hit_pos.At(hit_it)/pitchsize +96;
    //  distance2segment = (pos - seg_eta);
    interstrippos_approx= (qleft-qright)/qsum;
    posoverpitchsize = pos/pitchsize;
    interstrippos = (pos>0?1:-1)*(fabs(posoverpitchsize- int(posoverpitchsize))-0.5);
    //~ qpeak2pos = Muon->hit_qpeak2pos.At(hit_it);
    //~ qpeak2 = Muon->hit_qpeak2.At(hit_it);
    pstrip = Muon->hit_pstrip.At(hit_it);

}

void Hit::Dump(){
    std::cout << "Hit '"<< HitNumber <<"' "<< std::endl;
    if (measphi)    std::cout << "pstrip eta "<<pstrip<< std::endl;
    else            std::cout << "pstrip phi "<<pstrip<< std::endl;
    std::cout << "sector     "<<m_sector<< std::endl;
    std::cout << "s(re)fit       "<<sfit<<", ("<<srefit<<")"<< std::endl;
    //~ std::cout << " =+=+=+=+=+=+=+=+= "<< std::endl;
}


Hit::~Hit(){


}

