#include <vector>
#include <TColor.h>
#include <iomanip>
#include <iostream>
#include "CSCntuple/HelperFunctions.h"
#include <boost/filesystem.hpp>

Color_t NiceColor(TString selections){

    for (uint i = 0 ; i<20; ++i)if (StatusFromCode(i) == selections) return NiceColor(i);
    
    return 1;
}


Color_t NiceColor(uint i) {
    if (!i) return 1;
    std::vector <Color_t> colors = {
        kRed,
        kOrange-2,
        kGreen,
        kBlue,
        kTeal,
        kMagenta,
        kMagenta+3,
        kPink,
        kCyan+3,
        kRed+3,
        kGreen+3,
        kGray+2,
        kOrange+1,
        kOrange+7,
        kAzure+1
    };
    Color_t color = colors.at((i-1)%colors.size());
    //~ std::cout << "color is set to: " << color<< std::endl;
    return color;

}



TString replace(TString oldstr, const TString from, const TString to) {
    std::string newstr = (std::string) oldstr;
    size_t start_pos = newstr.find(from);
    while (start_pos != std::string::npos){
        newstr.replace(start_pos, ((std::string)from).length(), to);
        start_pos = newstr.find(from);
    }
    return newstr;
}

int CountDirContent (TString directory)
{
    namespace fs = boost::filesystem;
	fs::path Path(directory);
	int Nb_ext = 0;
	fs::directory_iterator end_iter; // Default constructor for an iterator is the end iterator

	for (fs::directory_iterator iter(Path); iter != end_iter; ++iter)
			++Nb_ext;

	return Nb_ext;
}

TString timestamp(){

    time_t now = time(0);
    tm *ltm = localtime(&now);
    std::stringstream ss;

    ss << std::setw(2) << std::setfill('0')<<1900+ltm->tm_year-2000;
    ss << std::setw(2) << std::setfill('0')<< ltm->tm_mon+1;
    ss << std::setw(2) << std::setfill('0')<<ltm->tm_mday;
    TString stamp = ss.str();
    std::cout << "time" << stamp << std::endl;
    return stamp;
}

TString StatusFromCode(int sfit, bool convert){

    TString result = "";

    switch (sfit){
    case    0:    result =     "unspoiled"; break;
    case    1:    result =     "simple"; break;
    case    2:    result =     "edge"; break;
    case    3:    result =     "multiple peaks"; break;
    case    4:    result =     "narrow"; break;
    case    5:    result =     "wide"; break;
    case    6:    result =     "skewed"; break;
    case    7:    result =     "inconsistent QRAT fits"; break;
    case    8:    result =     "stripfit failed"; break;
    case    9:    result =     "saturated"; break;
    case    10:    result =     "unspoiled with split"; break;
    case    11:    result =     "simple with split"; break;
    case    12:    result =     "edge with split"; break;
    case    13:    result =     "multiple peaks with split"; break;
    case    14:    result =     "narrow with split"; break;
    case    15:    result =     "wide with split"; break;
    case    16:    result =     "skewed with split"; break;
    case    17:    result =     "inconsistent QRAT fits with split"; break;
    case    18:    result =     "stripfit failed with split"; break;
    case    19:    result =     "saturated with split"; break;
    case    255:    result =     "undefined"; break;
    default:    result =     TString("unknown")+std::to_string(sfit);break;
    }
    if(convert) result = replace(result," ","_");

    return result;
}

