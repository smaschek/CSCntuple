#include "CSCntuple/HistSvc.h"

#include <TROOT.h>
#include <TH2F.h>
#include <iostream>
HistSvc::HistSvc() :
m_histNameSvc(nullptr)
{
  TH1::AddDirectory(false);
  gROOT->SetBatch(true); // why?
}

HistSvc::~HistSvc() {
  //  if (m_hists) {delete m_hists; m_hists = 0;}
  this->Reset();
}

void HistSvc::Reset() {
  // Clear all hists from store, freeing up memory
  for (auto entry : m_hists) {
    if (entry.second) delete entry.second;
  }
  m_hists.clear();
}
void HistSvc::Dump () {
    for (auto entry : m_hists){
        std::cout << entry.first << "   --- ---   " << entry.second->GetEntries()<<std::endl;
    }
}

void HistSvc::Write(TFile* file) {
  for (auto entry : m_hists) {
    if (entry.second) {
        //~ std::cout << entry.first<< std::endl;
      std::size_t length = entry.first.rfind("/");
      if (length!=std::string::npos) {
        std::cout << entry.first << "   --- ---   " << entry.second->GetEntries()<<std::endl;
        std::string dir = entry.first.substr(0, length);
           std::cout << "mkdir          " << dir << std::endl;
        file->mkdir(dir.c_str());
        file->cd(dir.c_str());
        entry.second->Write(entry.first.substr(length+1).c_str());
      } else {
        file->cd();
        entry.second->Write();
      }
    }
  }
}

TH1* HistSvc::GetHist(const std::string& name){
  TH1* hist = nullptr;
  std::map<std::string, TH1*>::iterator it = m_hists.find(name);
  if (it != m_hists.end()) hist = it->second;
  return hist;
}

std::string HistSvc::GetName(const std::string& name) {
  if (m_histNameSvc) {
    return m_histNameSvc->getHistName(name);
  }
  return name;
}

TH1* HistSvc::BookHist(const std::string& prename, const std::string& title, int nbinsx, float xlow, float xup) {
  std::string name = GetName(prename);
  TH1* hist = GetHist(name);
  if (hist == 0) {
    hist = new TH1F(name.c_str(), title.c_str(), nbinsx, xlow, xup);
    hist->Sumw2();
    m_hists[name] = hist;
  }
  return hist;
}


void HistSvc::BookFillHist(const std::string& name, const std::string& title, int nbinsx, float xlow, float xup, float value, float weight) {
  BookHist(name, title, nbinsx, xlow, xup)->Fill(value, weight);
}

TH1* HistSvc::BookHist(const std::string& prename, const std::string& title, int nbinsx, float* xbins) {
  std::string name = GetName(prename);

  TH1* hist=GetHist(name);
  if(hist == 0) {
    hist = new TH1F(name.c_str(), title.c_str(), nbinsx, xbins);
    hist->Sumw2();
    m_hists[name]=hist;
  }
  return hist;
}

void HistSvc::BookFillHist(const std::string& name, const std::string& title, int nbinsx, float* xbins, float value, float weight) {
  BookHist(name, title, nbinsx, xbins) -> Fill(value, weight);
}

TH1* HistSvc::BookHist(const std::string& prename, const std::string& title, int nbinsx, float xlow, float xup, int nbinsy, float ylow, float yup) {
  std::string name = GetName(prename);
  TH1* hist=GetHist(name);
  if(hist == 0) {
    hist = new TH2F(name.c_str(), title.c_str(), nbinsx, xlow, xup, nbinsy, ylow, yup);
    hist->Sumw2();
    m_hists[name]=hist;
  }
  return hist;
}

void HistSvc::BookFillHist(const std::string& name, const std::string& title, int nbinsx, float xlow, float xup, int nbinsy, float ylow, float yup, float xvalue, float yvalue, float weight) {
  static_cast<TH2F*>(BookHist(name, title, nbinsx, xlow, xup, nbinsy, ylow, yup))->Fill(xvalue, yvalue, weight);
}

TH1* HistSvc::BookHist(const std::string& prename, const std::string& title, int nbinsx, float* xbins, int nbinsy, float* ybins) {
  std::string name = GetName(prename);
  TH1* hist=GetHist(name);
  if(hist == 0) {
    hist = new TH2F(name.c_str(), title.c_str(), nbinsx, xbins, nbinsy, ybins);
    hist->Sumw2();
    m_hists[name]=hist;
  }
  return hist;
}

void HistSvc::BookFillHist(const std::string& name, const std::string& title, int nbinsx, float* xbins, int nbinsy, float* ybins, float xvalue, float yvalue, float weight) {
  static_cast<TH2F*>(BookHist(name, title, nbinsx, xbins, nbinsy, ybins)) -> Fill(xvalue, yvalue, weight);
}

std::pair<int, TGraph*> HistSvc::GetGraph(const std::string& name){
  std::pair<int, TGraph*> graph = std::make_pair(0, nullptr);
  std::map<std::string, std::pair<int, TGraph*> >::iterator it = m_graphs.find(name);
  if (it != m_graphs.end()) graph = it->second;
  return graph;
}

std::pair<int, TGraph*> HistSvc::BookGraph(const std::string& prename, const std::string& title, int nbins) {
  std::string name = GetName(prename);
  std::pair<int, TGraph*> graph = GetGraph(name);
  if(graph.second == nullptr) {
    graph = std::make_pair<int, TGraph*>(0, new TGraph(nbins));
    m_graphs[name] = graph;
  }
  return graph;
}

void HistSvc::BookFillGraph(const std::string& name, const std::string& title, int nbins, float x, float y) {
  std::pair<int, TGraph*> graph = BookGraph(name, title, nbins);
  graph.second->SetPoint(graph.first, x, y);
  graph.first++;
}

