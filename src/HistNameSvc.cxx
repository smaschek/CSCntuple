#include "CSCntuple/HistNameSvc.h"

#include <iostream>

HistNameSvc::HistNameSvc()
 : m_sample("None")
 , m_jets("None")
 , m_variation("None")
{
    m_name.reserve(300);
    m_variations = {
        { "",                                               "Nominal"}, // backup
        { "XbbJetTree_Nominal",                             "Nominal"},
        { "Nominal",                                        "Nominal"},
        // systematic variations
        //medium correlation scheme
        { "XbbJetTree_JET_Comb_Baseline_Kin__1up",          "JETCombKinBase1up"},
        { "XbbJetTree_JET_Comb_Baseline_Kin__1down",        "JETCombKinBase1dn"},
        { "XbbJetTree_JET_Comb_Modelling_Kin__1up",         "JETCombKinMod1up"},
        { "XbbJetTree_JET_Comb_Modelling_Kin__1down",       "JETCombKinMod1dn"},
        { "XbbJetTree_JET_Comb_TotalStat_Kin__1up",         "JETCombKinStat1up"},
        { "XbbJetTree_JET_Comb_TotalStat_Kin__1down",       "JETCombKinStat1dn"},
        { "XbbJetTree_JET_Comb_Tracking_Kin__1up",          "JETCombKinTrk1up"},
        { "XbbJetTree_JET_Comb_Tracking_Kin__1down",        "JETCombKinTrk1dn"},
        { "XbbJetTree_JET_Rtrk_Baseline_Sub__1up",           "JETRtrkSubBase1up"},
        { "XbbJetTree_JET_Rtrk_Baseline_Sub__1down",         "JETRtrkSubBase1dn"},
        { "XbbJetTree_JET_Rtrk_Modelling_Sub__1up",          "JETRtrkSubMod1up"},
        { "XbbJetTree_JET_Rtrk_Modelling_Sub__1down",        "JETRtrkSubMod1dn"},
        { "XbbJetTree_JET_Rtrk_TotalStat_Sub__1up",          "JETRtrkSubStat1up"},
        { "XbbJetTree_JET_Rtrk_TotalStat_Sub__1down",        "JETRtrkSubStat1dn"},
        { "XbbJetTree_JET_Rtrk_Tracking_Sub__1up",           "JETRtrkSubTrk1up"},
        { "XbbJetTree_JET_Rtrk_Tracking_Sub__1down",         "JETRtrkSubTrk1dn"},
        { "XbbJetTree_JET_Rtrk_Baseline_Sub__1up",           "JETRtrkSubBase1up"},
        { "XbbJetTree_JET_Rtrk_Baseline_Sub__1down",         "JETRtrkSubBase1dn"},
        { "XbbJetTree_JET_Rtrk_Modelling_Sub__1up",          "JETRtrkSubMod1up"},
        { "XbbJetTree_JET_Rtrk_Modelling_Sub__1down",        "JETRtrkSubMod1dn"},
        { "XbbJetTree_JET_Rtrk_TotalStat_Sub__1up",          "JETRtrkSubStat1up"},
        { "XbbJetTree_JET_Rtrk_TotalStat_Sub__1down",        "JETRtrkSubStat1dn"},
        { "XbbJetTree_JET_Rtrk_Tracking_Sub__1up",           "JETRtrkSubTrk1up"},
        { "XbbJetTree_JET_Rtrk_Tracking_Sub__1down",         "JETRtrkSubTrk1dn"},
        { "XbbJetTree_JET_JMR",                             "JETJMR1up"},
        { "XbbJetTree_JET_JER",                             "JETJER1up"},
        //strong correlation scheme
        { "XbbJetTree_JET_Comb_Baseline_All__1down",        "JETBaseAll1dn"},
        { "XbbJetTree_JET_Comb_Baseline_All__1up",          "JETBaseAll1up"},
        { "XbbJetTree_JET_Comb_Modelling_All__1down",       "JETModAll1dn"},
        { "XbbJetTree_JET_Comb_Modelling_All__1up",         "JETModAll1up"},
        { "XbbJetTree_JET_Comb_TotalStat_All__1down",       "JETStatAll1dn"},
        { "XbbJetTree_JET_Comb_TotalStat_All__1up",         "JETStatAll1up"},
        { "XbbJetTree_JET_Comb_Tracking_All__1down",        "JETTrackAll1dn"},
        { "XbbJetTree_JET_Comb_Tracking_All__1up",          "JETTrackAll1up"},
        //
        { "FT_EFF_extrapolation__1up",                      "FTEExtB1up"},
        { "FT_EFF_extrapolation__1down",                    "FTEExtB1dn"},
        { "FT_EFF_extrapolation_from_charm__1up",           "FTEExtC1up"},
        { "FT_EFF_extrapolation_from_charm__1down",         "FTEExtC1dn"},
        // other flavour tagging uncertainties are added to map on-the-fly
        { "FT1up",                                          "FT1up"},
        { "FT1dn",                                          "FT1dn"},
        { "FTSF",                                          "FTSF"},
    };
}

std::string HistNameSvc::getHistName(const std::string& variable)
{

    m_name.clear();
    m_name += m_sample;
    m_name += "_";
    m_name += m_jets;
    m_name += "_";
    m_name += m_variation;
    m_name += "_";
    m_name += variable;
    return m_name;
}

 void HistNameSvc::setVariation(const std::string& variation)
{
    // check pre-defined systematics
    auto it = m_variations.find(variation);
    if (it != m_variations.end()) {
        m_variation = it->second;
        return;
    }
    // check b-tagging scale factors
    if (variation.find("FT_EFF_Eigen") != std::string::npos) {
        // FT_EFF_Eigen_Light_0_AntiKt2PV0TrackJets__1up
        std::string flavour = "X";
        std::string num = "X"; // variation.substr(i-2,1);
        if (variation.substr(13,5) == "Light") {
            flavour = "L";
            num = variation.substr(19, variation.find("_", 19) - 19);
        }
        if (variation.substr(13,1) == "C") {
            flavour = "C";
            num = variation.substr(15,1);
        }
        if (variation.substr(13,1) == "B") {
            flavour = "B";
            num = variation.substr(15,1);
        }
        std::string side = "os";
        if (variation.find("__1up") != std::string::npos) side = "up";
        if (variation.find("__1down") != std::string::npos) side = "dn";
        size_t i = variation.find("AntiKt");
        m_variation = "FTEEig" + flavour + num + side;

        // speed-up: cache systematics
        std::cout << "Adding variation " << variation << " as " << m_variation << std::endl;
        m_variations[variation] = m_variation;
        return;
    }
    std::cout << "WARNING: could not find variation \"" << variation << "\" in variation map.\n";
    return;
}



//         {      "JET_SR1_JET_EtaIntercalibration_NonClosure", ""},
//         {                                      "FATJET_JER", ""},
//         {                                      "FATJET_JMR", ""},
//         {                                      "FATJET_D2R", ""},
//         {                      "JET_19NP_JET_EffectiveNP_1", ""},
//         {                      "JET_19NP_JET_EffectiveNP_2", ""},
//         {                      "JET_19NP_JET_EffectiveNP_3", ""},
//         {                      "JET_19NP_JET_EffectiveNP_4", ""},
//         {                      "JET_19NP_JET_EffectiveNP_5", ""},
//         {              "JET_19NP_JET_EffectiveNP_6restTerm", ""},
//         {      "JET_19NP_JET_EtaIntercalibration_Modelling", ""},
//         {      "JET_19NP_JET_EtaIntercalibration_TotalStat", ""},
//         {     "JET_19NP_JET_EtaIntercalibration_NonClosure", ""},
//         {                    "JET_19NP_JET_Pileup_OffsetMu", ""},
//         {                   "JET_19NP_JET_Pileup_OffsetNPV", ""},
//         {                      "JET_19NP_JET_Pileup_PtTerm", ""},
//         {                 "JET_19NP_JET_Pileup_RhoTopology", ""},
//         {                 "JET_19NP_JET_Flavor_Composition", ""},
//         {                    "JET_19NP_JET_Flavor_Response", ""},
//         {                      "JET_19NP_JET_BJES_Response", ""},
//         {                "JET_19NP_JET_PunchThrough_MCTYPE", ""},
//         {              "JET_19NP_JET_SingleParticle_HighPt", ""},
//
//         {                                     "MUONS_SCALE", ""},
//         {                                        "MUONS_ID", ""},
//         {                                        "MUONS_MS", ""},
//         {                               "EG_RESOLUTION_ALL", ""},
//         {                                    "EG_SCALE_ALL", ""},
//
//         // weight variations
//         {                                   "MUON_EFF_STAT", ""},
//         {                                    "MUON_EFF_SYS", ""},
//         {                                   "MUON_ISO_STAT", ""},
//         {                                    "MUON_ISO_SYS", ""},
//         {               "EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR", ""},
//         {          "EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR", ""},
//         {             "EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR", ""},
//         {              "EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR", ""},
//         {                           "PH_EFF_ID_Uncertainty", ""},
//         {                    "MUON_EFF_TrigSystUncertainty", ""},
//         {                    "MUON_EFF_TrigStatUncertainty", ""},
//         {                                  "MUON_TTVA_STAT", ""},
//         {                                   "MUON_TTVA_SYS", ""},
//         {                       "PH_EFF_TRKISO_Uncertainty", ""},
//         // one sided variations
//         {                             "MUON_EFF_STAT_LOWPT", ""},
//         {                              "MUON_EFF_SYS_LOWPT", ""},
//         {                            "MET_SoftTrk_ResoPara", ""},
//         {                            "MET_SoftTrk_ResoPerp", ""},
//         {                           "MET_SoftTrk_ScaleDown", ""},
//         {                             "MET_SoftTrk_ScaleUp", ""},
//         {                            "MET_JetTrk_ScaleDown", ""},
//         {                              "MET_JetTrk_ScaleUp", ""},
//         {                                  "PH_Iso_DDonoff", ""},
//         {                               "JvtEfficiencyDown", ""},
//         {                                 "JvtEfficiencyUp", ""},
//         {                          "MET_SoftCalo_ScaleDown", ""},
//         {                            "MET_SoftCalo_ScaleUp", ""},
//         {                               "MET_SoftCalo_Reso", ""},
