#!/bin/bash

# to ensure that I'm doing the same steps each time I recompile. 
# requires first:
# cd ../../../build/; asetup 21.0.37,here,Athena

cd ../../../build/
sleep 5
cmake ../source/Projects/WorkDir/
sleep 5
make -j 32
if [[ $? != 0 ]] ; then exit 1 ; fi
sleep 5
source x86_64-slc6-gcc62-opt/setup.sh
sleep 5
cd ../source/CSCntuple/run
sleep 5
