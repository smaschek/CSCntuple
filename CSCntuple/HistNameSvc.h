#ifndef HistNameSvc_h
#define HistNameSvc_h

#include <string>
#include <map>

class HistNameSvc {

public:


protected:

  // name buffer
  std::string m_name;
  std::string m_sample;
  std::string m_jets;
  std::string m_variation;
  std::map<std::string, std::string> m_variations;

public:

  HistNameSvc();
  ~HistNameSvc() {};

  std::string getHistName(const std::string& variable);
  std::string getObjectType() { return m_sample; }
  std::string getSelection() { return m_jets; }
  std::string getVariation() { return m_variation; }

  void setHistName(const std::string& name) { m_name = name; }
  void setObjectType(const std::string& type) { m_sample = type; }
  void setSelection(const std::string& selection) { m_jets = selection; }
  void setVariation(const std::string& variation);

};

#endif
