#ifndef track_h
#define track_h
 
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include <TTreeReaderArray.h>


#include "CSCntuple/muon.h"
#include <vector>
#include <TLorentzVector.h>



class Segment;

class Track {
    public:
    Track(muon* mu,uint hit_It);
    ~Track();
    void Dump();
    uint m_TrackNumber;
    TLorentzVector P;
    private:
    uint m_segment_index;
    Segment* m_segment;
};



#endif
