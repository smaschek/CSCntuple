#ifndef segment_h
#define segment_h
 
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include <TTreeReaderArray.h>


#include "CSCntuple/muon.h"
#include "CSCntuple/Track.h"
#include "CSCntuple/Hit.h"
#include <vector>
#include <TLorentzVector.h>





struct SegmentHit{

int sector;
int wlay;
double posX;
double pos;
double posZ;
double dpos;
int pstrip;
double charge;
double time;
int sfit;

};


class Segment {
    public:
    Segment(muon* mu, uint seg_It,bool debug=0,bool useRefit=0);
    ~Segment();
    //~ // preliminary hacks
    bool m_debug=0;
    TString m_outputFileName;

    //~ uint HitToSeg(uint hit)
    bool m_useRefit;
    std::vector <Hit*> m_Phi_Hits;
    std::vector <Hit*> m_Hits;
    Track* m_Track;
    uint m_NSpoiled;
    uint m_NUnspoiled;
    uint SegmentNumber;
    void SetTrack();
    void SetHits();
    void SetPosition();
    void SetEta();
    void SetPhi();
    //public methodes
    public:
    int n_Hits;
    int m_sector;
    private:
    muon* Muon;
    // Position variables
    public:
    double m_hit_strip_eta;
    double m_hit_strip_phi;
    Hit* FindHit(SegmentHit segment_hit);
    public:
    void Dump();

};



#endif
