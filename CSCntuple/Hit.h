#ifndef hit_h
#define hit_h
 
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include <TTreeReaderArray.h>


#include "CSCntuple/muon.h"
#include <vector>
#include <TLorentzVector.h>

class Segment;

class Hit {
    public:
    Hit(muon* mu,uint hit_It,Segment* segment);
    ~Hit();
    void Dump();
    bool measphi;
    uint HitNumber;
    std::vector <float> Cluster;
    int m_sector;
    double nstr;
    int sfit;
    int srefit;
    double qleft;
    double qright;
    double qsum;
    double time;
    int wlay;
    // double qleftsum3;
    // double qleftsum;
    // double qrightsum3;
    // double qrightsum;
    double qpeak;
    //~ //double peakiness;
    //~ //std::vector <double> distance2segments;
    double pitchsize;
    double pos;
    // double distance2segment;
    double interstrippos_approx;
    double posoverpitchsize;
    double interstrippos;
    //~ int qpeak2pos;
    //~ double qpeak2;
    int pstrip;
    Segment* GetSegment(){return m_segment;}
    private:
    uint m_segment_index;
    Segment* m_segment;
    uint m_NUnassociatedHits;
    muon* Muon;
};




#endif
