#ifndef CSCntupleReader_h
#define CSCntupleReader_h

#include "CSCntuple/Segment.h"
#include <TCanvas.h>
#include <TChain.h>
#include <TF1.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TLorentzVector.h>
#include <TPad.h>
#include <TROOT.h>
#include <TSystem.h>
#include <TTreeReader.h>
#include <TTreeReaderArray.h>
#include <TTreeReaderValue.h>
#include "HistSvc.h"
#include "HistNameSvc.h"

#include <TString.h>
#include <ctime>
#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <stdio.h>
#include <string>
#include <vector>

#include "CSCntuple/HelperFunctions.h"




class CSCntupleReader {

public:
    CSCntupleReader();
    ~CSCntupleReader();

    bool m_isMC;
    bool m_debug;
    bool m_showEventDisplays;
    bool m_useRefit;
    bool m_Sfits;
    
    void Process();
    void SetInputFile(std::vector <TString> input) {
        m_inputFileName=input;
    }
    void SetInputTree(TString treename) {
        m_TreeName=treename;
    }
    void SetOutputFile(TString output) {
        m_outputFileName=output;
    }
    void SetVerbose(bool verbose=1) {
        m_debug=verbose;
    }
    void SetNevents(Long64_t nevents) {
        m_nevents = nevents;
    }
    void SetPtCut(double muonPtCut){m_muonPtCut=muonPtCut;}

private:

//workflow
    void    Initialize();
    void    HistInitialize();
    void    Execute();
    void    Finalize();
    
//member functions
    void FillHistograms(Segment* segment);
    void ShowEventDisplay(uint segment);
    bool passSegmentCriteria(Segment* segment);
    bool passEventSelection();

    
    
//member variables
    muon* Muon;
    std::vector <TString> m_inputFileName;
    TString m_TreeName;
    TString m_outputFileName;
    TFile*  m_inFile;
    TChain* m_inChain;
    TTree*  m_Tree;
    TFile*  m_outFile;
    HistSvc* m_histSvc;
    HistNameSvc* m_histNameSvc;
    std::vector <Segment* >  m_Segments;
    void GetSegments();
    int m_nevents;
    int total_entries;
    double m_muonPtCut;
    TString m_BaseSelection;




// public:
// 
// 
// 
//     std::vector <TString> SortSelections(TString var);
// 
//     void SetSelection() {};
//     std::vector <TString> m_Branchlist;

//     void WriteGraphs();
//     void Write1DGraphs();
//     void Write2DGraphs();
//     void WriteHistos();

    //~ bool passSegmentSelection(Segment* segment);
//     void AllEvents(bool useAllEvents) {
//         m_allEvents = useAllEvents;
//     }
//     std::pair <double, double> GetEtaPhiPosOfSeg(uint seg, bool useHits=true);
};




#endif




