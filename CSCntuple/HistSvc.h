#ifndef HistSvc_h
#define HistSvc_h

#include <TH1.h>
#include <TGraph.h>
#include <TFile.h>

#include <map>
#include <string>
#include <vector>

#include "HistNameSvc.h"

class HistSvc {

public:

  HistSvc();
  ~HistSvc();

  TH1* BookHist(const std::string& name, const std::string& title, int nbinsx, float xlow, float xup);
  TH1* BookHist(const std::string& name, const std::string& title, int nbinsx, float* xbins);
  TH1* BookHist(const std::string& name, const std::string& title, int nbinsx, float xlow, float xup, int nbinsy, float ylow, float yup);
  TH1* BookHist(const std::string& name, const std::string& title, int nbinsx, float* xbins, int nbinsy, float* ybins);

  void BookFillHist(const std::string& name, const std::string& title, int nbinsx, float xlow, float xup, float value, float weight=1);
  void BookFillHist(const std::string& name, const std::string& title, int nbinsx, float* xbins, float value, float weight=1);
  void BookFillHist(const std::string& name, const std::string& title, int nbinsx, float xlow, float xup, int nbinsy, float ylow, float yup, float xvalue, float yvalue, float weight=1);
  void BookFillHist(const std::string& name, const std::string& title, int nbinsx, float* xbins, int nbinsy, float* ybins, float xvalue,  float yvalue, float weight=1);

  std::pair<int, TGraph*> BookGraph(const std::string& name, const std::string& title, int nbinsx);
  void BookFillGraph(const std::string& name, const std::string& title, int nbins, float x, float y);
  void Dump();
  void Write(TFile* file);
  void Reset();
  void SetHistNameSvc(HistNameSvc* svc) { m_histNameSvc = svc; }
  TH1* GetHist(const std::string& name);
  std::pair<int, TGraph*> GetGraph(const std::string& name);

private:

  std::map<std::string, TH1*> m_hists ;
  std::map<std::string, std::pair<int, TGraph*> > m_graphs ;
  HistNameSvc* m_histNameSvc;
  std::string GetName(const std::string& name);

};

#endif
