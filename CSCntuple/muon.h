//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Nov 13 18:33:42 2017 by ROOT version 6.08/06
// from TTree muon/Muon Track Information
// found on file: ../data/user.smaschek.00308047.CSCntuple.1_EXT0/user.smaschek.12541507.EXT0._000025.ntuple.root
//////////////////////////////////////////////////////////

#ifndef muon_h
#define muon_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include <TTreeReaderArray.h>

// Headers needed by this particular selector
#include <vector>



class muon : public TSelector {
public :
   TTreeReader     fReader;  //!the tree reader
   TTree          *fChain = 0;   //!pointer to the analyzed TTree or TChain

   // Readers to access the data (delete the ones you do not need).
   TTreeReaderValue<Int_t> run = {fReader, "run"};
   TTreeReaderValue<Int_t> evt = {fReader, "evt"};
   TTreeReaderValue<Int_t> lumiBlockNum = {fReader, "lumiBlockNum"};
   TTreeReaderValue<Int_t> bcid = {fReader, "bcid"};
   TTreeReaderValue<Char_t> L1RD0 = {fReader, "L1RD0"};
   TTreeReaderValue<Char_t> L1Mu = {fReader, "L1Mu"};
   TTreeReaderValue<Int_t> L2Mu = {fReader, "L2Mu"};
   TTreeReaderValue<Int_t> EFMu = {fReader, "EFMu"};
   TTreeReaderArray<unsigned short> muAuthors = {fReader, "muAuthors"};
   TTreeReaderArray<float> trkPx = {fReader, "trkPx"};
   TTreeReaderArray<float> truth_pt = {fReader, "truth_pt"};
   TTreeReaderArray<float> truth_eta = {fReader, "truth_eta"};
   TTreeReaderArray<float> truth_phi = {fReader, "truth_phi"};
   TTreeReaderArray<float> trk_me_pt = {fReader, "trk_me_pt"};
   TTreeReaderArray<float> trk_me_pterr = {fReader, "trk_me_pterr"};
   TTreeReaderArray<float> trk_me_eta = {fReader, "trk_me_eta"};
   TTreeReaderArray<float> trk_me_phi = {fReader, "trk_me_phi"};
   TTreeReaderArray<float> trk_id_pt = {fReader, "trk_id_pt"};
   TTreeReaderArray<float> trk_id_pterr = {fReader, "trk_id_pterr"};
   TTreeReaderArray<float> trk_id_eta = {fReader, "trk_id_eta"};
   TTreeReaderArray<float> trk_id_phi = {fReader, "trk_id_phi"};
   TTreeReaderArray<float> trkPy = {fReader, "trkPy"};
   TTreeReaderArray<float> trkPz = {fReader, "trkPz"};
   TTreeReaderArray<float> trkP = {fReader, "trkP"};
   TTreeReaderArray<float> trkPt = {fReader, "trkPt"};
   TTreeReaderArray<float> trkEta = {fReader, "trkEta"};
   TTreeReaderArray<float> trkPVXQoverP = {fReader, "trkPVXQoverP"};
   TTreeReaderArray<float> trkPVXd0 = {fReader, "trkPVXd0"};
   TTreeReaderArray<float> trkPVXZ0 = {fReader, "trkPVXZ0"};
   TTreeReaderArray<float> trkPVXTheta = {fReader, "trkPVXTheta"};
   TTreeReaderArray<float> trkPVXPhi0 = {fReader, "trkPVXPhi0"};
   TTreeReaderArray<float> trkChisq = {fReader, "trkChisq"};
   TTreeReaderArray<unsigned char> trkDoF = {fReader, "trkDoF"};
   TTreeReaderArray<unsigned char> nBLYHit = {fReader, "nBLYHit"};
   TTreeReaderArray<unsigned char> nPixelHit = {fReader, "nPixelHit"};
   TTreeReaderArray<unsigned char> nSCTHit = {fReader, "nSCTHit"};
   TTreeReaderArray<unsigned char> nTRTHit = {fReader, "nTRTHit"};
   TTreeReaderArray<unsigned char> nTHTHit = {fReader, "nTHTHit"};
   TTreeReaderArray<unsigned char> nMDTHit = {fReader, "nMDTHit"};
   TTreeReaderArray<unsigned char> nTGCPHit = {fReader, "nTGCPHit"};
   TTreeReaderArray<unsigned char> nTGCEHit = {fReader, "nTGCEHit"};
   TTreeReaderArray<unsigned char> nRPCPHit = {fReader, "nRPCPHit"};
   TTreeReaderArray<unsigned char> nRPCEHit = {fReader, "nRPCEHit"};
   TTreeReaderArray<unsigned char> nCSCPHit = {fReader, "nCSCPHit"};
   TTreeReaderArray<unsigned char> nCSCEHit = {fReader, "nCSCEHit"};
   TTreeReaderArray<unsigned char> nMuSeg = {fReader, "nMuSeg"};
   TTreeReaderArray<unsigned char> hitToMuon = {fReader, "hitToMuon"};
   TTreeReaderArray<unsigned char> hitToSeg = {fReader, "hitToSeg"};
   TTreeReaderArray<short> hit_sector = {fReader, "hit_sector"};
   TTreeReaderArray<unsigned char> hit_wlay = {fReader, "hit_wlay"};
   TTreeReaderValue<std::vector <bool>> hit_measphi = {fReader, "hit_measphi"};
   TTreeReaderArray<unsigned char> hit_pstrip = {fReader, "hit_pstrip"};
   TTreeReaderArray<float> hitRecoTime = {fReader, "hitRecoTime"};
   TTreeReaderArray<float> hitbeforeT0CorrTime = {fReader, "hitbeforeT0CorrTime"};
   TTreeReaderArray<float> hitbeforeBPCorrTime = {fReader, "hitbeforeBPCorrTime"};
   TTreeReaderArray<float> hit_time = {fReader, "hit_time"};
   TTreeReaderArray<unsigned char> hit_sfit = {fReader, "hit_sfit"};
   TTreeReaderArray<unsigned char> hit_tfit = {fReader, "hit_tfit"};
   TTreeReaderArray<float> hit_pos = {fReader, "hit_pos"};
   TTreeReaderArray<float> hit_PhiPosReFit = {fReader, "hit_PhiPosReFit"};
   TTreeReaderArray<float> hit_dpos = {fReader, "hit_dpos"};
   TTreeReaderArray<unsigned char> hit_nstr = {fReader, "hit_nstr"};
   TTreeReaderArray<unsigned char> hit_str0 = {fReader, "hit_str0"};
   TTreeReaderArray<float> hit_qsum = {fReader, "hit_qsum"};

   TTreeReaderArray<std::vector<float>> ClusterShape = {fReader, "hit_clustershape"};

   TTreeReaderArray<float> hit_qpeak = {fReader, "hit_qpeak"};
   //~ TTreeReaderArray<float> hit_qpeak2 = {fReader, "hit_qpeak2"};
   //~ TTreeReaderArray<int> hit_qpeak2pos = {fReader, "hit_qpeak2pos"};
   TTreeReaderArray<float> hit_qleft = {fReader, "hit_qleft"};
   //~ TTreeReaderArray<float> hit_qleftsum = {fReader, "hit_qleftsum"};
   //~ TTreeReaderArray<float> hit_qleftsum3 = {fReader, "hit_qleftsum3"};
   //~ TTreeReaderArray<float> hit_qrightsum = {fReader, "hit_qrightsum"};
   //~ TTreeReaderArray<float> hit_qrightsum3 = {fReader, "hit_qrightsum3"};
   TTreeReaderArray<float> hit_qright = {fReader, "hit_qright"};
   TTreeReaderArray<float> hit_dqpeak = {fReader, "hit_dqpeak"};
   TTreeReaderArray<float> hit_dqleft = {fReader, "hit_dqleft"};
   TTreeReaderArray<float> hit_dqright = {fReader, "hit_dqright"};
   TTreeReaderValue<std::vector<bool>> hit_phase = {fReader, "hit_phase"};
   TTreeReaderArray<float> hit_posrefit = {fReader, "hit_posrefit"};
   TTreeReaderArray<float> hit_phiposrefit = {fReader, "hit_phiposrefit"};
   TTreeReaderArray<float> hit_dposrefit = {fReader, "hit_dposrefit"};
   TTreeReaderArray<float> hit_qfitsig = {fReader, "hit_qfitsig"};
   TTreeReaderArray<float> hit_qfitdiff = {fReader, "hit_qfitdiff"};
   TTreeReaderArray<unsigned char> hit_srefit = {fReader, "hit_srefit"};
   TTreeReaderArray<unsigned char> segToMuon = {fReader, "segToMuon"};
   TTreeReaderArray<float> seg_time = {fReader, "seg_time"};
   TTreeReaderArray<float> seg_chsq = {fReader, "seg_chsq"};
   TTreeReaderArray<float> seg_ndof = {fReader, "seg_ndof"};
   TTreeReaderArray<float> seg_gposx = {fReader, "seg_gposx"};
   TTreeReaderArray<float> seg_gposy = {fReader, "seg_gposy"};
   TTreeReaderArray<float> seg_gposz = {fReader, "seg_gposz"};
   TTreeReaderArray<float> seg_gdirx = {fReader, "seg_gdirx"};
   TTreeReaderArray<float> seg_gdiry = {fReader, "seg_gdiry"};
   TTreeReaderArray<float> seg_gdirz = {fReader, "seg_gdirz"};
   TTreeReaderArray<float> seg_posTheta = {fReader, "seg_posTheta"};
   TTreeReaderArray<float> seg_posEta = {fReader, "seg_posEta"};
   TTreeReaderArray<float> seg_posPhi = {fReader, "seg_posPhi"};
   TTreeReaderArray<float> seg_dirTheta = {fReader, "seg_dirTheta"};
   TTreeReaderArray<float> seg_dirEta = {fReader, "seg_dirEta"};
   TTreeReaderArray<float> seg_dirPhi = {fReader, "seg_dirPhi"};
   TTreeReaderArray<float> seg_etalocpos = {fReader, "seg_etalocpos"};
   TTreeReaderArray<float> seg_philocpos = {fReader, "seg_philocpos"};
   TTreeReaderArray<float> seg_etalocdir = {fReader, "seg_etalocdir"};
   TTreeReaderArray<float> seg_philocdir = {fReader, "seg_philocdir"};
   TTreeReaderArray<float> seg_dy = {fReader, "seg_dy"};
   TTreeReaderArray<float> seg_dz = {fReader, "seg_dz"};
   TTreeReaderArray<float> seg_day = {fReader, "seg_day"};
   TTreeReaderArray<float> seg_daz = {fReader, "seg_daz"};
   TTreeReaderArray<float> seg_eyz = {fReader, "seg_eyz"};
   TTreeReaderArray<float> seg_eyay = {fReader, "seg_eyay"};
   TTreeReaderArray<float> seg_eyaz = {fReader, "seg_eyaz"};
   TTreeReaderArray<float> seg_ezay = {fReader, "seg_ezay"};
   TTreeReaderArray<float> seg_ezaz = {fReader, "seg_ezaz"};
   TTreeReaderArray<float> seg_eayaz = {fReader, "seg_eayaz"};
   TTreeReaderArray<short> seg_sector = {fReader, "seg_sector"};
   TTreeReaderArray<std::vector<float>> seg_posesX = {fReader, "seg_posesX"};
   TTreeReaderArray<std::vector<float>> seg_poses = {fReader, "seg_poses"};
   TTreeReaderArray<std::vector<float>> seg_posesZ = {fReader, "seg_posesZ"};
   TTreeReaderArray<std::vector<float>> seg_dposes = {fReader, "seg_dposes"};
   TTreeReaderArray<std::vector<int>> seg_pstrips = {fReader, "seg_pstrips"};
   TTreeReaderArray<std::vector<float>> seg_charges = {fReader, "seg_charges"};
   TTreeReaderArray<std::vector<float>> seg_times = {fReader, "seg_times"};
   TTreeReaderArray<std::vector<int>> seg_sfits = {fReader, "seg_sfits"};
   //~ TTreeReaderValue<std::vector<bool>> brokenLink = {fReader, "brokenLink"};


   muon(TTree * /*tree*/ =0) { }
   virtual ~muon() { }
   virtual Int_t   Version() const { return 2; }
   virtual void    Begin(TTree *tree);
   virtual void    SlaveBegin(TTree *tree);
   virtual void    Init(TTree *tree);
   virtual Bool_t  Notify();
   virtual Bool_t  Process(Long64_t entry);
   virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
   virtual void    SetOption(const char *option) { fOption = option; }
   virtual void    SetObject(TObject *obj) { fObject = obj; }
   virtual void    SetInputList(TList *input) { fInput = input; }
   virtual TList  *GetOutputList() const { return fOutput; }
   virtual void    SlaveTerminate();
   virtual void    Terminate();

   //~ ClassDef(muon,0);

};

#endif

#ifdef muon_cxx
void muon::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the reader is initialized.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   fReader.SetTree(tree);
}

Bool_t muon::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}


#endif // #ifdef muon_cxx
