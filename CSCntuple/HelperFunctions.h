#ifndef HELPER
#define HELPER

#include <TString.h>
#include <TColor.h>

Color_t NiceColor(TString selection);
Color_t NiceColor(uint i);
TString replace(TString oldstr, const TString from, const TString to = "") ;
TString timestamp();
TString StatusFromCode(int sfit, bool convert = 0);
int CountDirContent (TString directory);
#endif
