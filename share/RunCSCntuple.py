############################################################
# RUNS AUTOCONFIG - no need to set tags
############################################################

#from AthenaCommon.AthenaCommonFlags import athenaCommonFlags #was Athena
from AthenaCommon.AppMgr import ServiceMgr as svcMgr #Athina
from AthenaCommon.Constants import VERBOSE, DEBUG, INFO, ERROR #Athina
#from AthenaCommon.AppMgr import theApp
import AthenaPoolCnvSvc.ReadAthenaPool #to read xAODS or ESDs

from AthenaCommon.AthenaCommonFlags import jobproperties as jp


print 'begin RunCSCntuple'

# Configure the CoreDumpSvc Athina
if not hasattr(svcMgr,"CoreDumpSvc"):
    from AthenaServices.Configurables import CoreDumpSvc
    svcMgr += CoreDumpSvc()

if not vars().has_key("NumberOfEvents"):
    NumberOfEvents = -1

print 'NumberOfEvents = ',NumberOfEvents
#if not vars().has_key("ReadMC"):

if not vars().has_key("FillClusterBranches"):
    FillClusterBranches = True

if not vars().has_key("MuonCollectionName"):
    MuonCollectionName = "Muons"

if not vars().has_key("NtupleFileName"):
    NtupleFileName = "ntuple.root"

if not vars().has_key("Debug"):#this does not do anything; I think it is overwritten by this: rec.OutputLevel.set_Value_and_Lock(DEBUG); 
    Debug = True

if not vars().has_key("Verbose"):
    Verbose = False

if not vars().has_key("MuonMomentumCut"):
    MuonMomentumCut = 0

print 'point 1'

theApp.EvtMax = 100

#if not vars().has_key("SkipEvents"):
#    SkipEvents = -1
#jp.AthenaCommonFlags.SkipEvents = 1000      
      
if not vars().has_key("InputFile"):
    InputFile = ""

if not vars().has_key("InputFiles"):
    if len(InputFile): InputFiles = [InputFile]
    else: InputFiles = []

    
if len("InputFiles"):
    jp.AthenaCommonFlags.FilesInput = InputFiles
    svcMgr.EventSelector.InputCollections = InputFiles

print 'point 2' 

from RecExConfig.RecFlags import rec

from AthenaCommon.GlobalFlags import globalflags #Athina, was GlobalFlags
globalflags.DetGeo='atlas'

#rec.doTrigger.set_Value_and_Lock(False) # leave false; nothing to do with trigger analysis  
#rec.doApplyAODFix.set_Value_and_Lock(True)
          
from GeoModelSvc.GeoModelSvcConf import GeoModelSvc
GeoModelSvc = GeoModelSvc()



print 'point 3'

# include your algorithm job options here
from AthenaCommon.AlgSequence import AlgSequence
job=AlgSequence()

rec.UserAlgs.set_Value_and_Lock("../share/CSCntupleJobOption.py")


# Output log setting; this is for the framework in general
# You may over-ride this in your job options for your algorithm
rec.OutputLevel.set_Value_and_Lock(DEBUG);	


# Write settings; keep all of these to false.
# Control the writing of your own n-tuple in the alg's job options
rec.doCBNT.set_Value_and_Lock(False)
rec.doWriteAOD.set_Value_and_Lock (False)
rec.doWriteTAG.set_Value_and_Lock (False)
rec.doHist.set_Value_and_Lock (False)

print 'point 4'


# main jobOption - must always be included
include ("RecExCommon/RecExCommon_topOptions.py")

print 'point 9'

# Following 3 lines needed for TAG jobs (as is)
#svcMgr.EventSelector.RefName= "StreamAOD"
#svcMgr.EventSelector.CollectionType="ExplicitROOT"
#svcMgr.EventSelector.Query = "" 

print 'Setting events to run'


#jp.AthenaCommonFlags.EvtMax = NumberOfEvents # number of event to process

# Stops writing of monitoring ntuples (big files)
#from PerfMonComps.PerfMonFlags import jobproperties as jp
#jp.PerfMonFlags.doMonitoring = False
#jp.PerfMonFlags.doFastMon = False

