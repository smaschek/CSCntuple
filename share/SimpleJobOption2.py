from RecExConfig.RecFlags import rec
from AthenaCommon.AthenaCommonFlags import athenaCommonFlags as acf

import os.path

fileList=InputFiles

#if not os.path.exists(fileList[0]):
#	print "FATAL ERROR: file can not be found, earlier test probably failed" 
#	print "missing files:", fileList
#	exit(1) 

from AthenaCommon.AthenaCommonFlags import athenaCommonFlags
athenaCommonFlags.FilesInput=fileList

from RecExConfig.RecFlags import rec
rec.doAOD.set_Value_and_Lock(False)
rec.doWriteAOD.set_Value_and_Lock(False)
rec.doCBNT.set_Value_and_Lock(False)

acf.EvtMax=-1

if not vars().has_key("NumberOfEvents"):
        NumberOfEvents = -1
if NumberOfEvents > -1 :
        acf.EvtMax =  NumberOfEvents



if not ('OutputLevel' in dir()):
	rec.OutputLevel=VERBOSE#INFO

rec.readESD=True
rec.doCBNT=False
rec.doTrigger=False
rec.doWriteAOD=False
rec.doWriteESD=False
rec.doWriteTAG=False
rec.doAOD=False 
rec.doESD=False 
doTAG=False

rec.doTruth=True

include("RecExCommon/RecExCommon_topOptions.py")

isMC = False
isESD = True
print "Using stream isMC, isESD: ", isMC, isESD

from AthenaCommon.AlgSequence import AlgSequence, AthSequencer
job = AlgSequence()
job += AthSequencer("ModSequence1")



from GeoModelSvc.GeoModelSvcConf import GeoModelSvc
GeoModelSvc = GeoModelSvc()

from AthenaCommon.GlobalFlags import globalflags
globalflags.DataSource='data'

globalflags.DetGeo = 'atlas'

from CSCntuple.CSCntupleConf import CreateNtuple
job.ModSequence1 += CreateNtuple('CreateNtuple') #here the new ntuple is created

job.ModSequence1.CreateNtuple.readESD = isESD
job.ModSequence1.CreateNtuple.readMC             = isMC
job.ModSequence1.CreateNtuple.outputNTupleName   = NtupleFileName

#get rid of messages and increase speed
Service ("StoreGateSvc" ).ActivateHistory=False


print AlgSequence
print ServiceMgr



