print 'begin CSCSpoiled_JobOption'

from RecExConfig.RecFlags import rec
from AthenaCommon.AthenaCommonFlags import athenaCommonFlags as acf

import os.path

fileList=InputFiles

DetDescrVersion = 'ATLAS-GEO-20-00-01'





#if not os.path.exists(fileList[0]):
#   print "FATAL ERROR: file can not be found, earlier test probably failed"
#   print "missing files:", fileList
#   exit(1)



    #~ # was only available in CSC JobOptions
#~ from RecExConfig.InputFilePeeker import inputFileSummary
#~ isMC = False if inputFileSummary['evt_type'][0] == 'IS_DATA'  else True
#~ isESD = True if "StreamESD" in inputFileSummary['stream_names'] else False
#~ print "Using stream isMC, isESD: ", isMC, isESD

from AthenaCommon.AthenaCommonFlags import athenaCommonFlags
from AthenaCommon.GlobalFlags import jobproperties

from GeoModelSvc.GeoModelSvcConf import GeoModelSvc
from RecExConfig.RecFlags import rec

athenaCommonFlags.FilesInput=fileList
rec.doAOD.set_Value_and_Lock(False)
rec.doWriteAOD.set_Value_and_Lock(False)
rec.doCBNT.set_Value_and_Lock(False)

acf.EvtMax=NumberOfEvents

if not ('OutputLevel' in dir()):
    rec.OutputLevel=VERBOSE#INFO

rec.readESD=True
rec.doCBNT=False
rec.doTrigger=False
rec.doWriteAOD=False
rec.doWriteESD=False
rec.doWriteTAG=False
rec.doAOD=False
rec.doESD=False
doTAG=False
rec.doTruth=True



jobproperties.Global.DetDescrVersion = DetDescrVersion
GeoModelSvc = GeoModelSvc()


include("RecExCommon/RecExCommon_topOptions.py")

print "Using stream isMC, isESD: ", isMC, isESD

from AthenaCommon.AlgSequence import AlgSequence, AthSequencer
job = AlgSequence()
job += AthSequencer("ModSequence1")





from AthenaCommon.GlobalFlags import globalflags

if not isMC:
    globalflags.DataSource='data'

globalflags.DetGeo = 'atlas'


#-------------------------------------------------------------
#  #here the new ntuple is created
#-------------------------------------------------------------
# ~ from AthenaCommon.CfgGetter import getPublicTool
from CSCntuple.CSCntupleConf import CreateNtuple
job.ModSequence1 += CreateNtuple('CreateNtuple')
job.ModSequence1.CreateNtuple.readESD = isESD
job.ModSequence1.CreateNtuple.readMC             = isMC
job.ModSequence1.CreateNtuple.outputNTupleName   = NtupleFileName
job.ModSequence1.CreateNtuple.fillClusterBranches= FillClusterBranches
job.ModSequence1.CreateNtuple.muonMomentumCut    = MuonMomentumCut
job.ModSequence1.CreateNtuple.MuonCollectionName = MuonCollectionName
job.ModSequence1.CreateNtuple.ClusterRefitter = ClusterRefitter
# ~ bla = getPublicTool("LogRatCscClusterFitter")
#~ job.ModSequence1.CreateNtuple.SkipEvents         = SkipEvents
job.ModSequence1.CreateNtuple.OutputLevel        = DEBUG
if Debug:   job.ModSequence1.CreateNtuple.OutputLevel = DEBUG
if Verbose: job.ModSequence1.CreateNtuple.OutputLevel = VERBOSE

#get rid of messages and increase speed
Service ("StoreGateSvc" ).ActivateHistory=False


print AlgSequence
print ServiceMgr


#~ from CscClusterization.QratCscClusterFitter import QratCscClusterFitter
##ToolSvc.QratCscClusterFitter.max_width    = [100,100,100,100]
#ToolSvc.QratCscClusterFitter.OutputLevel = VERBOSE
#ToolSvc.QratCscClusterFitter.qrat_maxdiff = 1000.
##ToolSvc.QratCscClusterFitter.qrat_maxsig  = 6.
##ToolSvc.QratCscClusterFitter.position_option_eta = "ATANH"
#ToolSvc.QratCscClusterFitter.position_option_phi = "NONE"
#ToolSvc.QratCscClusterFitter.error_option_eta = "CHARGE"
#ToolSvc.QratCscClusterFitter.error_option_phi = "NONE"
#ToolSvc.QratCscClusterFitter.error_tantheta = 0.57
#ToolSvc.QratCscClusterFitter.xtan_css_eta_offset = 0.0015
#ToolSvc.QratCscClusterFitter.xtan_css_eta_slope = 0.000137
#ToolSvc.QratCscClusterFitter.xtan_csl_eta_offset = -.0045
#ToolSvc.QratCscClusterFitter.xtan_csl_eta_slope = 0.000131
#ToolSvc.QratCscClusterFitter.qratmin_css_eta = 0.0940459




#~ # was only available in CSC JobOptions
#~ # This is still picked up in new schema of retrieval method...
#~ from AthenaCommon.CfgGetter import getPublicTool

#~ import CscSegmentMakers.CscSegmentMakersConf
#~ from CscSegmentMakers.CscSegmentMakersConf import CscSegmentUtilTool
#~ #job += getPublicTool("CscSegmentUtilTool")
#~ #job += getPublicTool("CscSegmentUtilTool",checkType=False)


print 'end CSCSpoiled_JobOption'





