from AthenaCommon.AppMgr import ServiceMgr as svcMgr #Athina
from AthenaCommon.Constants import VERBOSE, DEBUG, INFO, ERROR #Athina
#from AthenaCommon.AppMgr import theApp
import AthenaPoolCnvSvc.ReadAthenaPool #to read xAODS or ESDs

from AthenaCommon.AthenaCommonFlags import jobproperties as jp

print 'begin SimpleJobOption'

# Configure the CoreDumpSvc Athina
if not hasattr(svcMgr,"CoreDumpSvc"):
    from AthenaServices.Configurables import CoreDumpSvc
    svcMgr += CoreDumpSvc()

from AthenaCommon.GlobalFlags import GlobalFlags
GlobalFlags.DetGeo='atlas'

MuonCollectionName = "Muons"

if not vars().has_key("NtupleFileName"):
    NtupleFileName = "ntuple.root"

if not vars().has_key("FillClusterBranches"):
    FillClusterBranches = True

Debug = True

jp.AthenaCommonFlags.FilesInput = InputFiles
svcMgr.EventSelector.InputCollections = InputFiles

#from RecExConfig.InputFilePeeker import inputFileSummary
#isMC = False if inputFileSummary['evt_type'][0] == 'IS_DATA'  else True
#isESD = True if "StreamESD" in inputFileSummary['stream_names'] else False
isMC = False
isESD = True
print "Using stream isMC, isESD: ", isMC, isESD

from AthenaCommon.AlgSequence import AlgSequence, AthSequencer
job = AlgSequence()
job += AthSequencer("ModSequence1")

GlobalFlags.DataSource='data'

from GeoModelSvc.GeoModelSvcConf import GeoModelSvc
GeoModelSvc = GeoModelSvc()

from CSCntuple.CSCntupleConf import CreateNtuple
job.ModSequence1 += CreateNtuple('CreateNtuple') #here the new ntuple is created

job.ModSequence1.CreateNtuple.readESD = isESD
job.ModSequence1.CreateNtuple.readMC             = isMC
job.ModSequence1.CreateNtuple.outputNTupleName   = NtupleFileName
job.ModSequence1.CreateNtuple.fillClusterBranches= FillClusterBranches



if Debug:
    job.ModSequence1.CreateNtuple.OutputLevel = DEBUG

if Verbose:
    job.ModSequence1.CreateNtuple.OutputLevel = VERBOSE


from RecExConfig.RecFlags import rec
rec.OutputLevel.set_Value_and_Lock(DEBUG);

# Write settings; keep all of these to false.
# Control the writing of your own n-tuple in the alg's job options
rec.doCBNT.set_Value_and_Lock(False)
rec.doWriteAOD.set_Value_and_Lock (False)
rec.doWriteTAG.set_Value_and_Lock (False)
rec.doHist.set_Value_and_Lock (False)

include ("RecExCommon/RecExCommon_topOptions.py")
