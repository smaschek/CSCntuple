//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Mar 25 15:55:06 2015 by ROOT version 5.34/24
// from TTree csc_segment/list of any segment
// found on file: CosmicMuons.ExtCSCValid.root
//////////////////////////////////////////////////////////

#ifndef CscSegment_h
#define CscSegment_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "/var/build/71/x86_64-slc6-gcc48-opt/projects/ROOT-5.34.24/src/ROOT/5.34.24/cint/cint/lib/prec_stl/vector"
#include "/var/build/71/x86_64-slc6-gcc48-opt/projects/ROOT-5.34.24/src/ROOT/5.34.24/cint/cint/lib/prec_stl/vector"
#include <vector>
#include <vector>

// Fixed size dimensions of array or collections stored in the TTree if any.

class CscSegment {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   Int_t           run;
   Int_t           evt;
   Int_t           lumiBlockNum;
   Int_t           bcid;
   Char_t          L1RD0;
   Char_t          L1Mu;
   Int_t           L2Mu;
   Int_t           EFMu;
   vector<float>   *gposx;
   vector<float>   *gposy;
   vector<float>   *gposz;
   vector<float>   *gdirx;
   vector<float>   *gdiry;
   vector<float>   *gdirz;
   vector<float>   *r;
   vector<float>   *phi;
   vector<float>   *eta;
   vector<short>   *segToMuon;
   vector<short>   *sector;
   vector<float>   *time;
   vector<vector<int> > *pstrips;
   vector<vector<float> > *charges;
   vector<vector<float> > *times;

   // List of branches
   TBranch        *b_run;   //!
   TBranch        *b_evt;   //!
   TBranch        *b_lumiBlockNum;   //!
   TBranch        *b_bcid;   //!
   TBranch        *b_RD0;   //!
   TBranch        *b_L1Mu;   //!
   TBranch        *b_L2Mu;   //!
   TBranch        *b_EFMu;   //!
   TBranch        *b_gposx;   //!
   TBranch        *b_gposy;   //!
   TBranch        *b_gposz;   //!
   TBranch        *b_gdirx;   //!
   TBranch        *b_gdiry;   //!
   TBranch        *b_gdirz;   //!
   TBranch        *b_r;   //!
   TBranch        *b_phi;   //!
   TBranch        *b_eta;   //!
   TBranch        *b_segToMuon;   //!
   TBranch        *b_sector;   //!
   TBranch        *b_time;   //!
   TBranch        *b_pstrips;   //!
   TBranch        *b_charges;   //!
   TBranch        *b_times;   //!

   CscSegment(TTree *tree=0);
   virtual ~CscSegment();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef CscSegment_cxx
CscSegment::CscSegment(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("CosmicMuons.ExtCSCValid.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("CosmicMuons.ExtCSCValid.root");
      }
      f->GetObject("csc_segment",tree);

   }
   Init(tree);
}

CscSegment::~CscSegment()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t CscSegment::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t CscSegment::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void CscSegment::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   gposx = 0;
   gposy = 0;
   gposz = 0;
   gdirx = 0;
   gdiry = 0;
   gdirz = 0;
   r = 0;
   phi = 0;
   eta = 0;
   segToMuon = 0;
   sector = 0;
   time = 0;
   pstrips = 0;
   charges = 0;
   times = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("run", &run, &b_run);
   fChain->SetBranchAddress("evt", &evt, &b_evt);
   fChain->SetBranchAddress("lumiBlockNum", &lumiBlockNum, &b_lumiBlockNum);
   fChain->SetBranchAddress("bcid", &bcid, &b_bcid);
   fChain->SetBranchAddress("L1RD0", &L1RD0, &b_RD0);
   fChain->SetBranchAddress("L1Mu", &L1Mu, &b_L1Mu);
   fChain->SetBranchAddress("L2Mu", &L2Mu, &b_L2Mu);
   fChain->SetBranchAddress("EFMu", &EFMu, &b_EFMu);
   fChain->SetBranchAddress("gposx", &gposx, &b_gposx);
   fChain->SetBranchAddress("gposy", &gposy, &b_gposy);
   fChain->SetBranchAddress("gposz", &gposz, &b_gposz);
   fChain->SetBranchAddress("gdirx", &gdirx, &b_gdirx);
   fChain->SetBranchAddress("gdiry", &gdiry, &b_gdiry);
   fChain->SetBranchAddress("gdirz", &gdirz, &b_gdirz);
   fChain->SetBranchAddress("r", &r, &b_r);
   fChain->SetBranchAddress("phi", &phi, &b_phi);
   fChain->SetBranchAddress("eta", &eta, &b_eta);
   fChain->SetBranchAddress("segToMuon", &segToMuon, &b_segToMuon);
   fChain->SetBranchAddress("sector", &sector, &b_sector);
   fChain->SetBranchAddress("time", &time, &b_time);
   fChain->SetBranchAddress("pstrips", &pstrips, &b_pstrips);
   fChain->SetBranchAddress("charges", &charges, &b_charges);
   fChain->SetBranchAddress("times", &times, &b_times);
   Notify();
}

Bool_t CscSegment::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void CscSegment::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t CscSegment::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef CscSegment_cxx
