#!/usr/bin/env python

import sys,os
from AthenaCommon.Include import include#Athina

#def include(filename):
#    if os.path.exists(filename): 
#        execfile(filename)
#    else:
#        print 'file/path does not exist'

myname = "runJobs.py: "
mygenfiles = [
########################################################################### 
# Check CSC ESD files: Urgent (athena version; 21.0.15)
# Comment the following lines if you want to run on the grid!!!
###########################################################################
#"/afs/cern.ch/user/e/ekarentz/work/ESD_CSC/myCSC.mu15fixed.ESD.pool.root"
#"/ptmp/mpp/smaschek/QualificationTask/ESDs/0927/myCSC.mu20fixed.rel21.0.15.1.1.ESD.pool.root"
#"/afs/cern.ch/user/s/smaschek/public/myCSC.mu20fixed.rel21-0-37.0.v2.ESD.pool.root"
"/afs/cern.ch/work/f/fsforza/public/Ntup/MCP/data16_13TeV.00308047.physics_Main.merge.DESDM_MCP.r9264_p3082_p3082/DESDM_MCP.11087515._000504.pool.root.1"
#"root://eosatlas.cern.ch//eos/atlas/user/j/jomeyer/CSC_study/myCSC.mu20fixed.ESD.pool.root"
#"root://eosatlas.cern.ch//eos/atlas/user/j/jomeyer/CSC_study/myCSC.mu25fixed.ESD.pool.root"
#"root://eosatlas.cern.ch//eos/atlas/user/j/jomeyer/CSC_study/myCSC.mu30fixed.ESD.pool.root"
#"root://eosatlas.cern.ch//eos/atlas/user/j/jomeyer/CSC_study/myCSC.mu35fixed.ESD.pool.root"
#"root://eosatlas.cern.ch//eos/atlas/user/j/jomeyer/CSC_study/myCSC.mu40fixed.ESD.pool.root"
#"root://eosatlas.cern.ch//eos/atlas/user/j/jomeyer/CSC_study/myCSC.mu45fixed.ESD.pool.root"
#"root://eosatlas.cern.ch//eos/atlas/user/j/jomeyer/CSC_study/myCSC.mu50fixed.ESD.pool.root"
#####
# ESD file for tracks only in CSC region:
#"/afs/cern.ch/user/j/jomeyer/workarea/public/myCSC.mu20fixed.ESD.pool.root"
#
]


InputFiles = mygenfiles
ReadMC = False # default is false
outbase = "extCSCevlid"

#ListFile = 'DESDMMS'
ListFile = 'CSC_ESD'

# Local use:
NtupleFileName = ListFile + "." + outbase + ".root"

# Grid use:
#NtupleFileName = "13TeV_Muons.ExtCSCValid.root"
#Uncomment the following line if you want to run on the Grid:
#NtupleFileName = "13TeV_full_Muons.ExtCSCValid.root"
#NtupleFileName = "ExtCSCValid_from_ESD.root"

NumberOfEvents = 1000
#SkipEvents = 1000
Verbose =False

include("../share/SimpleJobOption2.py")
