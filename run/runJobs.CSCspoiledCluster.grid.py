#!/usr/bin/env python


 #   
 #   
 #   
 #   

import sys,os
from AthenaCommon.Include import include#Athina

myname = "runJobs.grid.py: "

#~ InputFile = "data16_13TeV.00308047.physics_Main.merge.DESDM_MCP.r9264_p3082_p3082"
#~ inDS = "data16_13TeV.00308047.physics_Main.merge.DESDM_MCP.r9264_p3082_p3082"




NumberOfEvents = -1

# this is overwritten if sent to grid, here: for local tests
InputFile = "/ptmp/mpp/smaschek/data/DESDM/data16_13TeV.00308047.physics_Main.merge.DESDM_MCP.r9264_p3082_p3082/DESDM_MCP.11087515._000244.pool.root.1"
InputFiles = [InputFile]



isMC = False
isESD = True

MuonMomentumCut         = 100000
FillClusterBranches     = False
NtupleFileName          = "ntuple.root"
SkipEvents              = 0
Verbose                 = True
Debug                   = True
ReadMC                  = False
MuonCollectionName      = "Muons"
include("CSCntuple/CSCSpoiled_JobOption.py")


